/**
 * Copyright (C) 2022 Ubports Foundation.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License version 3, as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranties of MERCHANTABILITY,
 * SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *  Lionel Duboeuf <lduboeuf@ouvaton.org>
 */

#include <QDBusConnection>

#include "cellbroadcastprivateadaptor.h"

QMap<QString, CellBroadcastPrivate*> cellBroadcastData;


CellBroadcastPrivate::CellBroadcastPrivate(QObject *parent) :
    QObject(parent)
{
    QDBusConnection::sessionBus().registerObject(OFONO_MOCK_CELLBROADCAST_OBJECT, this);
    QDBusConnection::sessionBus().registerService("org.ofono");

    new CellBroadcastAdaptor(this);
}

QVariantMap CellBroadcastPrivate::GetProperties()
{
    return mProperties;
}

void CellBroadcastPrivate::SetProperty(const QString &name, const QDBusVariant& value)
{
    qDebug() << "CellBroadcastPrivate::SetProperty" << name << value.variant();
    mProperties[name] = value.variant();
    Q_EMIT PropertyChanged(name, value);
}

void CellBroadcastPrivate::MockCellBroadcastMessage(const QString &text, quint16 channel)
{
    qDebug() << "kikou MockCellBroadcastMessage" << text << channel;
    Q_EMIT IncomingBroadcast(text, channel);
}

void CellBroadcastPrivate::MockCellBroadcastETWSMessage(const QString &text, const QString &type, bool alert, bool popup)
{
    QVariantMap properties;
    properties["EmergencyType"] = type;
    properties["EmergencyAlert"] = alert;
    properties["Popup"] = popup;
    Q_EMIT EmergencyBroadcast(text, properties);
}
