/**
 * Copyright (C) 2022 Ubports Foundation.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License version 3, as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranties of MERCHANTABILITY,
 * SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *  Lionel Duboeuf <lduboeuf@ouvaton.org>
 */

#ifndef CELLBROADCASTPRIVATE_H
#define CELLBROADCASTPRIVATE_H

#include <QObject>
#include <QDBusContext>
#include <QDBusObjectPath>
#include "mock_common.h"

class CellBroadcastPrivate : public QObject, protected QDBusContext {

    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.ofono.CellBroadcast")
public:
    explicit CellBroadcastPrivate(QObject *parent = nullptr);

Q_SIGNALS:
    void PropertyChanged(const QString &name, const QDBusVariant &value);
    void TopicsChanged(const QString &);
    void IncomingBroadcast(const QString &, quint16);
    void EmergencyBroadcast(const QString &, const QVariantMap &);


public Q_SLOTS:
    void MockCellBroadcastMessage(const QString &text, quint16 channel);
    void MockCellBroadcastETWSMessage(const QString &text, const QString &type, bool alert, bool popup);

    QVariantMap GetProperties();
    void SetProperty(const QString &name, const QDBusVariant &value);
private:
    QVariantMap mProperties;
};

extern QMap<QString, CellBroadcastPrivate*> cellBroadcastData;


#endif // CELLBROADCASTPRIVATE_H
