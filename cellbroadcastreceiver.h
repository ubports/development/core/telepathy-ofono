/**
 * Copyright (C) 2022 Ubports Foundation.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License version 3, as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranties of MERCHANTABILITY,
 * SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *  Lionel Duboeuf <lduboeuf@ouvaton.org>
 */

#ifndef CELLBROADCASTRECEIVER_H
#define CELLBROADCASTRECEIVER_H

#include <QObject>

#include "cellbroadcast-types.h"
#include <ofonocellbroadcast.h>

class CellBroadcastReceiver : public QObject
{
    Q_OBJECT
public:
    explicit CellBroadcastReceiver(OfonoModem::SelectionSetting modemSetting, const QString &modemPath, QObject *parent = nullptr);
    ~CellBroadcastReceiver();


Q_SIGNALS:
    void incomingCellBroadcast(const QString &message, CellBroadcast::Type type, quint16 channel);

private Q_SLOTS:
    void onCellBroadcastEmergencyMessage(const QString &message, const QVariantMap &info);
    void onCellBroadcastIncomingMessage(const QString &message, quint16 channel);

private:
    OfonoCellBroadcast *mOfonoCellBroadcast;
};


#endif // CELLBROADCASTRECEIVER_H
