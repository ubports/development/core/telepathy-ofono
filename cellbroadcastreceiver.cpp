/**
 * Copyright (C) 2022 Ubports Foundation.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License version 3, as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranties of MERCHANTABILITY,
 * SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *  Lionel Duboeuf <lduboeuf@ouvaton.org>
 */

#include "cellbroadcastreceiver.h"

#include <QDebug>
#include <QList>

static QList<quint16> LEVEL2_TOPICS = {
       CellBroadcast::CMAS_ALERT_EXTREME_IMMEDIATE_OBSERVED,
       CellBroadcast::CMAS_ALERT_EXTREME_IMMEDIATE_LIKELY,
       CellBroadcast::CMAS_ALERT_EXTREME_IMMEDIATE_OBSERVED_LANGUAGE,
       CellBroadcast::CMAS_ALERT_EXTREME_IMMEDIATE_LIKELY_LANGUAGE,

};


static QList<quint16> LEVEL3_TOPICS = {
    CellBroadcast::CMAS_ALERT_EXTREME_EXPECTED_LIKELY,
    CellBroadcast::CMAS_ALERT_EXTREME_EXPECTED_OBSERVED,
    CellBroadcast::CMAS_ALERT_EXTREME_EXPECTED_OBSERVED_LANGUAGE,
    CellBroadcast::CMAS_ALERT_EXTREME_EXPECTED_LIKELY_LANGUAGE,
    CellBroadcast::CMAS_ALERT_SEVERE_IMMEDIATE_OBSERVED,
    CellBroadcast::CMAS_ALERT_SEVERE_IMMEDIATE_LIKELY,
    CellBroadcast::CMAS_ALERT_SEVERE_EXPECTED_OBSERVED,
    CellBroadcast::CMAS_ALERT_SEVERE_EXPECTED_LIKELY,
    CellBroadcast::CMAS_ALERT_SEVERE_IMMEDIATE_OBSERVED_LANGUAGE,
    CellBroadcast::CMAS_ALERT_SEVERE_IMMEDIATE_LIKELY_LANGUAGE,
    CellBroadcast::CMAS_ALERT_SEVERE_EXPECTED_OBSERVED_LANGUAGE,
    CellBroadcast::CMAS_ALERT_SEVERE_EXPECTED_LIKELY_LANGUAGE
};

CellBroadcastReceiver::CellBroadcastReceiver(OfonoModem::SelectionSetting modemSetting, const QString &modemPath, QObject *parent)
{
    mOfonoCellBroadcast = new OfonoCellBroadcast(modemSetting, modemPath);
    QObject::connect(mOfonoCellBroadcast, SIGNAL(emergencyBroadcast(QString,QVariantMap)), this, SLOT(onCellBroadcastEmergencyMessage(QString,QVariantMap)));
    QObject::connect(mOfonoCellBroadcast, SIGNAL(incomingBroadcast(QString,quint16)), this, SLOT(onCellBroadcastIncomingMessage(QString,quint16)));
}

CellBroadcastReceiver::~CellBroadcastReceiver()
{
    mOfonoCellBroadcast->deleteLater();
}

// ETWS cell broadcast
void CellBroadcastReceiver::onCellBroadcastEmergencyMessage(const QString &message, const QVariantMap &info)
{
    bool isEmergencyAlert = info["EmergencyAlert"].toBool();
    bool isPopup = info["Popup"].toBool();
    QString emergencyType = info["EmergencyType"].toString();

    CellBroadcast::Type type;
    if (isEmergencyAlert && isPopup) {
        type = CellBroadcast::TYPE_LEVEL_1;
    } else if (isEmergencyAlert) {
        type = CellBroadcast::TYPE_LEVEL_2;
    } else {
        type = CellBroadcast::TYPE_LEVEL_3;
    }

    quint16 topic;
    if (emergencyType == "Earthquake") {
        topic = CellBroadcast::ETWS_ALERT_EARTHQUAKE;
    } else if (emergencyType == "Tsunami") {
        topic = CellBroadcast::ETWS_ALERT_TSUNAMI;
    } else if (emergencyType == "Earthquake+Tsunami") {
        topic = CellBroadcast::ETWS_ALERT_EARTHQUAKE_AND_TSUNAMI;
    } else {
        topic = CellBroadcast::ETWS_ALERT_OTHER;
    }

    Q_EMIT incomingCellBroadcast(message, type, topic);
}

void CellBroadcastReceiver::onCellBroadcastIncomingMessage(const QString &message, quint16 channel)
{
    qDebug() << "CellBroadcastReceiver onCellBroadcastIncomingMessage" << message << channel;
    CellBroadcast::Type type;
    if (channel == CellBroadcast::CMAS_ALERT_PRESIDENTIAL_LEVEL || channel == CellBroadcast::CMAS_ALERT_PRESIDENTIAL_LEVEL_LANGUAGE) {
        type = CellBroadcast::TYPE_LEVEL_1;
    } else if (LEVEL2_TOPICS.contains(channel)) {
        type = CellBroadcast::TYPE_LEVEL_2;
    } else if (LEVEL3_TOPICS.contains(channel)) {
        type = CellBroadcast::TYPE_LEVEL_3;
    } else if (channel == CellBroadcast::CMAS_ALERT_PUBLIC_SAFETY) {
        type = CellBroadcast::TYPE_LEVEL_4;
    } else if (channel == CellBroadcast::CMAS_ALERT_CHILD_ABDUCTION_EMERGENCY) {
        type = CellBroadcast::TYPE_AMBER;
    } else if (channel == CellBroadcast::CMAS_ALERT_REQUIRED_MONTHLY_TEST) {
        type = CellBroadcast::TYPE_MONTHLY_TEST;
    } else if (channel == CellBroadcast::CMAS_ALERT_STATE_LOCAL_TEST) {
        type = CellBroadcast::TYPE_TEST;
    } else if (channel == CellBroadcast::CMAS_ALERT_EXERCISE) {
        type = CellBroadcast::TYPE_EXERCISE;
    } else if (channel == CellBroadcast::CMAS_ALERT_OPERATOR_DEFINED_USE) {
        type = CellBroadcast::TYPE_RESERVED;
    } else {
        type = CellBroadcast::TYPE_OTHER;
    }


    Q_EMIT incomingCellBroadcast(message, type, channel);
}
